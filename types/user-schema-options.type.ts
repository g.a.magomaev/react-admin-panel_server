import { SchemaTypeOptions } from 'mongoose';

export type TUserSchemaOptions = {
    id: SchemaTypeOptions<number>,
    email: SchemaTypeOptions<string>,
    password: SchemaTypeOptions<string>,
}