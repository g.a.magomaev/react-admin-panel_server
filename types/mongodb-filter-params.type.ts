export type TMongoDBFilterParams<T> = {
    [key: string]: { $in: T }
}