import { SchemaTypeOptions } from 'mongoose';

export type TDeviceSchemaOptions = {
    id: SchemaTypeOptions<number>,
    room_id: SchemaTypeOptions<number>,
    name: SchemaTypeOptions<string>,
    type: SchemaTypeOptions<string>,
    description: SchemaTypeOptions<string>
}