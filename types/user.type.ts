export type TUser = {
  email: string;
  password: string;
  id?: number;
};
