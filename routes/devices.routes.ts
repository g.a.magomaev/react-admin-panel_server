import { Router, Response, Request } from "express";
import { CallbackError, Document } from 'mongoose';

import { TDevice } from '../types/device.type';
import { Utils } from '../utilities/utils';
import { TDeviceSchemaOptions } from '../types/device-schema-options.type';
import { DeviceModel } from '../models/device.model';

const router: Router = Router();

// Get all devices including search and filter

router.get("/devices", async (req: Request, res: Response) => {

  try {
    let result: Document<TDeviceSchemaOptions>[];

    const { search } = req.query;
    const { filter } = req.query;

    if (search) {
      result = await DeviceModel.find({ name: { $regex: search, $options: 'ig' } });
    } else if (filter) {
      result = await DeviceModel.find(Utils.getMongoDBFilterParams(filter as string));
    } else {
      result = await DeviceModel.find();
    }

    res.json(result);

  } catch (error) {
    console.log(error);
    res.status(500).json({ message: 'Some problem' });
  }

});

// Create device

router.post("/device", async (req: Request, res: Response) => {

  try {
    const newDevice: TDevice = req.body;
    const hasMatches: Document<TDeviceSchemaOptions> | null = await DeviceModel.collection.findOne({ name: newDevice.name });

    if (hasMatches) {
      return res.status(401).json({
        status: 400,
        message: `The device with name: ${newDevice.name} is exist.`
      })
    }

    const [lastDevice]: Document<TDeviceSchemaOptions>[] = await DeviceModel.find().sort({ "id": -1 }).limit(1);

    newDevice.id = lastDevice ? lastDevice.id + 1 : 1;

    await DeviceModel.collection.insertOne(req.body, (err: CallbackError) => {
      if (err) {
        return res.status(400).send({
          status: 400,
          message: err.message,
        })
      }
      return res.status(201).json({
        status: 201,
        message: `The device with name: ${newDevice.name} was added.`
      })
    }
    );


  } catch (error) {
    res.status(400).json(error);
  }

});

// Delete device

// DELETE
router.delete("/device/:id", async (req: Request, res: Response) => {

  const { id } = req.params;
  const hasMatches: Document<TDeviceSchemaOptions> | null = await DeviceModel.findOne({ id });

  try {
    if (hasMatches) {
      await DeviceModel.deleteOne({ id }, {}, (err) => {
        return res.status(200).json({
          status: 200,
          message: `The device with id: ${id} was deleted.`
        })
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: `The device with id: ${id} is not exist.`
      })
    }

  } catch (error) {
    res.json(error);
  }


});

// UPDATE
router.put("/device", async (req: Request, res: Response) => {
  const device: any = req.body;

  try {
    const editingDevice: Document<TDeviceSchemaOptions> | null = await DeviceModel.findOne({ id: device.id });

    if (editingDevice) {
      const hasMatches: Document<TDeviceSchemaOptions> | null = await DeviceModel.findOne({ name: device.name });

      if (hasMatches) {
        return res.status(400).json({
          status: 400,
          message: `The device with name: ${device.name} is exist.`
        })
      }

      editingDevice.update(device, {}, (err: CallbackError) => {
        if (err) {
          return res.status(400).send({
            status: 400,
            message: err.message,
          })
        }
        return res.status(200).send({
          status: 200,
          message: `The device "${device.name}" was updated`,
        })
      }
      )
    }
  } catch (error) {
    res.json(error);
  }
});

module.exports = router;
