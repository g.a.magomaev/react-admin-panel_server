import { Application } from "express";

const modulesRoutes: {
  basePath: string;
  rotePath: string;
}[] = [
    { basePath: '/', rotePath: './devices.routes' },
    { basePath: '/', rotePath: './users.routes' },
  ]

export const appRouter: (app: Application) => void = (app: Application) => {
  modulesRoutes.forEach(({ basePath, rotePath }) => { app.use(basePath, require(rotePath)) })
};
