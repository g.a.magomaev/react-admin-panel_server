import { TUserSchemaOptions } from '../types/user-schema-options.type';
import { Response, Request, Router } from "express";
import { CallbackError, Document } from "mongoose";
import * as jwt from "jsonwebtoken";
import { TUser } from '../types/user.type';
import { UserModel } from '../models/user.model';

const router: Router = Router();

// REGISTER + CREATE
router.post("/auth/register", async (req: Request, res: Response) => {
  try {
    const usersCollection: Document<TUserSchemaOptions> | null = await UserModel.findOne();
    if (!usersCollection) {
      return res.status(401).json({
        status: 400,
        message: "Users collection can't be load"
      })
    }

    const newUser: TUser = req.body;
    const emailMatches: Document<TUserSchemaOptions> | null = await usersCollection.collection.findOne({ email: newUser.email });

    if (emailMatches) {
      return res.status(401).json({
        status: 400,
        message: `The user with email: ${newUser.email} is exist.`
      })
    }

    const [lastUser]: Document<TUserSchemaOptions>[] = await UserModel.find().sort({ "id": -1 }).limit(1);

    newUser.id = lastUser.id + 1;

    await usersCollection.collection.insertOne(req.body, (err: CallbackError) => {
      if (err) {
        return res.status(400).send({
          status: 400,
          message: err.message,
        })
      }
      return res.status(201).json({
        status: 201,
        message: `The user with email: ${newUser.email} was registered.`
      })
    }
    );


  } catch (error) {
    res.status(400).json(error);
  }
});

// LOGIN
router.post("/auth/login", async (req: Request, res: Response) => {

  try {
    const usersCollection: Document<TUserSchemaOptions> | null = await UserModel.findOne();
    if (!usersCollection) {
      return res.status(401).json({
        status: 400,
        message: "Users collection can't be load"
      })
    }

    const { email, password }: TUser = req.body;
    const hasUser: Document<TUserSchemaOptions> | null = await usersCollection.collection.findOne({ email });

    if (!hasUser) {
      return res.status(401).json({
        status: 400,
        message: `The user with email: ${email} is not exist0. Please, register.`
      })
    }


    const passwordMatches: Document<TUserSchemaOptions> | null = await usersCollection.collection.findOne({ password });

    if (!passwordMatches) {
      return res.status(400).send({
        status: 400,
        message: "The password is incorrect",
      });
    }

    res.status(200).send({
      status: 200,
      message: "Access is allowed",
      data: {
        id: hasUser.id,
        token: jwt.sign({ foo: "bar" }, "shahs")
      },
    });

  } catch (error) {
    res.status(400).json(error);
  }
});

module.exports = router;