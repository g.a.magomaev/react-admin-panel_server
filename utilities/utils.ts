import { TMongoDBFilterParams } from '../types/mongodb-filter-params.type';

export class Utils {

    static getMongoDBFilterParams(filterParamsStr: string) {

        const filterParams: { [key: string]: string[] | number[] } = JSON.parse(filterParamsStr);
        const mongoDBFilterParams: TMongoDBFilterParams<string[] | number[]> = JSON.parse(filterParamsStr);
        Object.keys(filterParams).forEach((key: string) => { mongoDBFilterParams[key] = { $in: filterParams[key] } });
        return mongoDBFilterParams;
    }

}
