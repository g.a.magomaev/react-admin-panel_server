
import { TDeviceSchemaOptions } from '../types/device-schema-options.type';
import { Model, model, Document } from 'mongoose';
import { DeviceSchema } from '../schemas/device.schema';

export const DeviceModel: Model<Document<TDeviceSchemaOptions>> = model('Device', DeviceSchema);
