import { TUserSchemaOptions } from '../types/user-schema-options.type';
import { Model, model, Document } from 'mongoose';
import { UserSchema } from '../schemas/user.schema';

export const UserModel: Model<Document<TUserSchemaOptions>> = model('User', UserSchema);
