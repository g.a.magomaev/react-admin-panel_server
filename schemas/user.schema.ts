import { TUserSchemaOptions } from '../types/user-schema-options.type';
import { Schema, Model, Document } from 'mongoose';

export const UserSchema: Schema<Document<TUserSchemaOptions>, Model<Document<TUserSchemaOptions>>> = new Schema({
    id: {
        type: Number,
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'This field is required'],
        trim: true
    },
    password: {
        type: String,
        required: [true, 'This field is required'],
        trim: true,
        minlength: [6, 'Min length must be more then six symbols']
    }
}, {
    timestamps: true,
});

