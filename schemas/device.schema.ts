
import { TDeviceSchemaOptions } from '../types/device-schema-options.type';
import { Schema, Model, Document } from 'mongoose';

export const DeviceSchema: Schema<Document<TDeviceSchemaOptions>, Model<Document<TDeviceSchemaOptions>>> = new Schema({
    id: {
        type: Number,
        required: [true, 'This field is required'],
        min: [1, 'Minimum length is one symbol']
    },
    room_id: {
        type: Number,
        required: [true, 'This field is required'],
        min: [1, 'Minimum length is one symbol']
    },
    name: {
        type: String,
        unique: true,
        required: [true, 'This field is required'],
        trim: true
    },
    type: {
        type: String,
        required: [true, 'This field is required'],
        trim: true
    },
    description: {
        type: String,
        required: [true, 'This field is required'],
        trim: true
    }
}, {
    timestamps: true,
});
