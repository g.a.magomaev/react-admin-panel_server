import express, { Application, Request, Response } from "express";
import { connect, connection } from 'mongoose';
import config from 'config';
import bodyParser from "body-parser";
import { appRouter } from './routes/app.routes';

const app: Application = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const PORT: number = config.get('port');

// CORSE
app.use((req: Request, res: Response, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, Content-Type, X-Auth-Token'"
  );
  res.setHeader("Access-Control-Allow-Credentials", "true");
  next();
});

connect(config.get('mongoURI'), {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})

connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
})

app.listen(PORT, () => {
  console.log(`Server has been started on port: ${PORT}.`)
})

appRouter(app);
